from django.core.management.base import BaseCommand

import app.internal.bot as bot


class Command(BaseCommand):
    help = "Command to run telegram bot"

    def handle(self, *args, **options):
        bot.run_bot()
