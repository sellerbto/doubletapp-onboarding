from django.contrib import admin

from app.internal.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class AdminTelegramUser(admin.ModelAdmin):
    list_display = ("chat_id", "username", "full_name", "phone_number")
