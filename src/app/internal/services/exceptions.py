class NoPhoneNumber(Exception):
    """
    :raises Когда нет номера телефона в базе данных и пользователь отправляют команду, кроме /start и /set_phone
    """

    pass
