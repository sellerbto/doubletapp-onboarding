from app.internal.models.telegram_user import TelegramUser


async def async_update_or_create_tg_user(chat_id: int, username: str, full_name: str) -> (TelegramUser, bool):
    return await TelegramUser.objects.aupdate_or_create(
        chat_id=chat_id, defaults={"username": username, "full_name": full_name}
    )


async def async_get_user_info(chat_id: int) -> str:
    tg_user = await async_get_tg_user(chat_id)
    return f"""Вот что я знаю о вас:
            Полное имя: {tg_user.full_name}
            Ник: @{tg_user.username}
            Номер телефона: {tg_user.phone_number}
            Уникальный id телеграмм: {tg_user.chat_id}
            """


async def async_get_tg_user(chat_id: int) -> TelegramUser:
    return await TelegramUser.objects.aget(chat_id=chat_id)


def get_tg_user(chat_id: int) -> TelegramUser:
    return TelegramUser.objects.get(chat_id=chat_id)
