from django.urls import path

from app.internal.transport.rest.handlers import UserInfoView

urlpatterns = [path("me/", UserInfoView.as_view())]
