from telegram import KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import ContextTypes

from app.internal.models.telegram_user import TelegramUser
from app.internal.services.exceptions import NoPhoneNumber
from app.internal.services.user_service import async_get_tg_user, async_get_user_info, async_update_or_create_tg_user
from app.internal.transport.bot.messages import BotMessages


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = update.effective_user
    tg_user, created_now = await async_update_or_create_tg_user(user.id, user.username, user.full_name)
    if created_now:
        await update.message.reply_text(f"{BotMessages.unknown_user_greeting}, {user.full_name}!")
    else:
        await update.message.reply_text(f"{BotMessages.known_user_greeting}, {user.full_name}!")


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await async_get_tg_user(update.effective_user.id)
    await update.effective_user.send_message(
        text=BotMessages.set_phone_instruction,
        reply_markup=ReplyKeyboardMarkup([[KeyboardButton(text=BotMessages.set_phone_menu, request_contact=True)]]),
    )


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(await async_get_user_info(update.effective_user.id))


async def contact_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    tg_user = await async_get_tg_user(update.effective_user.id)
    tg_user.phone_number = update.message.contact.phone_number
    await tg_user.asave()
    await update.effective_user.send_message(text=BotMessages.phone_saved, reply_markup=ReplyKeyboardRemove())


async def error_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if type(context.error) is TelegramUser.DoesNotExist:
        await update.message.reply_text(BotMessages.unknown_user_error)
    elif type(context.error) is NoPhoneNumber:
        await update.message.reply_text(BotMessages.no_phone_error)
    else:
        await update.message.reply_text(BotMessages.unknown_error)
