from concurrent.futures import ThreadPoolExecutor

from telegram.ext.filters import Message, MessageFilter

from app.internal.services.exceptions import NoPhoneNumber
from app.internal.services.user_service import get_tg_user


class UserWithPhoneFilter(MessageFilter):
    def filter(self, message: Message) -> bool:
        with ThreadPoolExecutor() as executor:  # Это необходимо, чтобы поток не блокировался.
            future = executor.submit(get_tg_user, message.chat_id)
            tg_user = future.result()
        if not tg_user.phone_number:
            raise NoPhoneNumber()
        return True
