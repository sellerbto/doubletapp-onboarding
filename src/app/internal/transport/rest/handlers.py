from http import HTTPStatus

from django.core.serializers.python import Serializer
from django.http import HttpRequest, JsonResponse
from django.views import View

from app.internal.models.telegram_user import TelegramUser
from app.internal.services.user_service import async_get_tg_user
from app.internal.transport.rest.errors import UserInfoViewErrors


class UserInfoView(View):
    async def get(self, request: HttpRequest) -> JsonResponse:
        if "chat-id" not in request.headers:
            return UserInfoViewErrors.missing_chat_id_header()
        chat_id = request.headers.get("chat-id")
        if not chat_id.isdigit():
            return UserInfoViewErrors.chat_id_must_contain_only_digits()
        try:
            user = await async_get_tg_user(chat_id)
            if user.phone_number is None:
                return UserInfoViewErrors.missing_mobile_phone()
            serialized_user_fields = Serializer().serialize([user])[0]["fields"]
            return JsonResponse(serialized_user_fields, status=HTTPStatus.OK)
        except TelegramUser.DoesNotExist:
            return UserInfoViewErrors.user_not_exists()
