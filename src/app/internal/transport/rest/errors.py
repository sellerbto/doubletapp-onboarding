from http import HTTPStatus

from django.http import JsonResponse


class UserInfoViewErrors:
    @staticmethod
    def missing_chat_id_header():
        return JsonResponse({"message": "Please provide chat-id header"}, status=HTTPStatus.BAD_REQUEST)

    @staticmethod
    def chat_id_must_contain_only_digits():
        return JsonResponse({"message": "chat-id header must contain only digits"}, status=HTTPStatus.BAD_REQUEST)

    @staticmethod
    def missing_mobile_phone():
        return JsonResponse({"message": "Please provide mobile phone"}, status=HTTPStatus.FORBIDDEN)

    @staticmethod
    def user_not_exists():
        return JsonResponse(
            {"message": "Telegram user with chat-id you provided does not exists"}, status=HTTPStatus.NOT_FOUND
        )
