from django.core.validators import MinLengthValidator
from django.db import models


class TelegramUser(models.Model):
    chat_id = models.PositiveIntegerField(
        unique=True,
        verbose_name="Chat id",
    )
    username = models.CharField(
        blank=True,
        max_length=32,
        validators=[MinLengthValidator(5)],
        verbose_name="Ник",
        null=True,
    )
    full_name = models.CharField(
        max_length=128,
        validators=[MinLengthValidator(1)],
        verbose_name="Полное имя",
    )
    phone_number = models.CharField(
        blank=True,
        max_length=15,
        verbose_name="Номер телефона",
        null=True,
    )

    def __str__(self):
        return f"{self.full_name} Id чата:{self.chat_id}"
