from telegram import Update
from telegram.ext import Application, CommandHandler, MessageHandler
from telegram.ext.filters import CONTACT

from app.internal.transport.bot.filters import UserWithPhoneFilter
from app.internal.transport.bot.handlers import contact_handler, error_handler, me, set_phone, start
from config.settings import env


def run_bot() -> None:
    application = Application.builder().token(env("TELEGRAM_TOKEN")).build()
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("set_phone", set_phone))
    application.add_handler(CommandHandler("me", me, filters=UserWithPhoneFilter()))
    application.add_handler(MessageHandler(CONTACT, contact_handler))
    application.add_error_handler(error_handler)
    application.run_polling(allowed_updates=Update.ALL_TYPES)
