_Сервер может сам обслуживать static файлы при debug=False с помощью whitenoise_

# Как запустить?
- заполнить .env файл

```shell
cp .env.example .env
```

```shell
pipenv install
```

```shell
pipenv shell
```
Следующие команды должны выполняться в созданном pipenv env:

```shell
make makemigrations
```

```shell
make migrate
```

```shell
make collectstatic
```

```shell
make createsuperuser
```

```shell
make dev
```

```shell
make run_bot
```

Эндпоинт **me** расположен по адресу: base_url/api/me/
